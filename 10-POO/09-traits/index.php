<?php

// un Trait sirve para compartir metodos entre clases sin heredar directamente del padre
trait utilidades
{
    public function mostrarNombre()
    {
        echo "<h1>El nombre es " . $this->nombre . "</h1>";
    }
}

class person
{
    public $nombre;
    public $apellidos;

    // USE nos permite utilizar los metodos de los traits
    use utilidades;
}

class auto
{
    public $nombre;
    public $modelo;

    use utilidades;
}

class videoJuego
{
    public $nombre;
    public $anio;

    use utilidades;
}

$person = new person();
$auto = new auto();
$juego = new videoJuego();

$person->nombre = 'Luis Barraza';
echo $person->mostrarNombre();
