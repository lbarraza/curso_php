<?php


class user
{
    // Una constante se define de la siguiente manera
    // const nombre_constante = valor;
    const URL_COMPLETA = "http://localhost";
    public $email;
    public $password;


    function getEmail()
    {
        return $this->email;
    }

    function setEmail($email)
    {
        $this->email = $email;
    }

    function getPassword()
    {
        return $this->password;
    }

    function setPassword($password)
    {
        $this->password = $password;
    }
}

$user = new user;
// Mostrar por pantalla el valor de una constante
echo $user::URL_COMPLETA;

var_dump($user);