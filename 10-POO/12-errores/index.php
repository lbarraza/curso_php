<?php

// Una excepcion es un fallo
// Capturar excepciones, en codigo susceptible a errores.
try {
    throw new Exception('"Hay un error de logica"', 1);
} catch (Exception $e) {
    echo "Se encontro el siguiente error: ". $e->getMessage();
} /* finally {   
    // El finally se ejecuta al terminar de evaluar todo, normalmente no suele usarse.
    echo "Todo correcto";
} */
echo "<hr>";

try {
    if(isset($_GET['id'])){
        echo "<h1>El parametro es: {$_GET['id']}</h1>";
    }else {
        throw new Exception('Faltan parametros por la URL');
    }
} catch (Exception $e) {
    echo "Se encontro el siguiente error: ". $e->getMessage();
}