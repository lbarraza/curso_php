<?php
// Programacion orientada a objetos


// Definir una clase (Molde para crear mas objetos de tipo coche con caracteristicas parecidas)

class Auto
{
    // atributos o propiedades (Variables)
    public $color;
    public $marca;
    public $modelo;
    public $velocidad;
    public $caballos_fuerza;

    // El constructor nos permite enviarle los valores de las prodiedades al llamar la clase
    public function __construct($color, $marca, $modelo, $velocidad = 480, $caballos_fuerza)
    {
        $this->color = $color;
        $this->marca = $marca;
        if ($velocidad > 480) {
         $this->velocidad = $velocidad;
        } else {
            $this->velocidad = 480;
        }
        $this->modelo = $modelo;
        $this->velocidad = $velocidad;
        $this->caballos_fuerza = $caballos_fuerza;
    }

    // Metodos, son acciones que hace el objeto (funciones)
    public function getColor()
    {
        // Busca en esta clase la propiedad "X"
        return $this->color;
    }

    public function setColor($color)
    {
        $this->color = $color;
    }

    public function acelerar()
    {
        $this->velocidad++;
    }

    public function frenar()
    {
        $this->velocidad--;
    }
    public function getVelocidad()
    {
        return $this->velocidad;
    }
    public function setModelo($modelo)
    {
        $this->modelo = $modelo;
    }
} // fin de la clase



