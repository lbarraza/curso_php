<?php
namespace MisClases;

class entrada
{
    public $titulo;

    public $fecha;

    public function __construct()
    {
        $this->titulo = 'Review de GTA 5';
        $this->fecha = '4 de mayo de 2021';
    }

    function getTitulo()
    {
        return $this->titulo;
    }

    function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    }

    function getFecha()
    {
        return $this->fecha;
    }

    function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }
}
