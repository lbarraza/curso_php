<?php
namespace MisClases;

class usuario
{
    public $nombre;

    public $email;

    public function __construct()
    {
        $this->nombre = 'Luis';
        $this->email = 'luisfdopoker@hotmail.com';
    }

    function getNombre()
    {
        return $this->nombre;
    }

    function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    function getEmail()
    {
        return $this->email;
    }

    function setEmail($email)
    {
        $this->email = $email;
    }
}
