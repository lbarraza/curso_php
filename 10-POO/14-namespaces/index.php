<?php
require_once 'autoload.php';

class principal  
{
    public $usuario;

    public $categoria;

    public $entrada;

    public function __construct($usuario, $categoria, $entrada)
    {
        $this->usuario = $usuario;
        $this->categoria = $categoria;
        $this->entrada = $entrada;
    }    
}
