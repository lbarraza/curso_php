<?php
// HERENCIA: La posibilidad de compartir atributo y metodos entre clases.

class person
{
    public $nombre;

    public $apellidos;

    public $edad;

    public $altura;

    public $peso;

    public function __construct()
    {
        $this->nombre = 'Luis';
        $this->apellidos = 'Barraza';
        $this->edad = 29;
        $this->altura = 1.75;
        $this->peso = 75;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    public function getApellidos()
    {
        return $this->apellidos;
    }

    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;
    }

    public function getEdad()
    {
        return $this->edad;
    }

    public function setEdad($edad)
    {
        $this->edad = $edad;
    }

    public function getAltura()
    {
        return $this->altura;
    }

    public function setAltura($altura)
    {
        $this->altura = $altura;
    }

    public function getPeso()
    {
        return $this->peso;
    }

    public function setPeso($peso)
    {
        $this->peso = $peso;
    }

    public function hablar()
    {
        return "Estoy hablando";
    }

    public function caminar()
    {
        return "Estoy caminando";
    }
}

// Para heredar las propiedades de la clase padre se agrega extends NombreClase a la clase creada.
// class claseCreada extends ClasePadre
class informatico extends person
{
    public $lenguajes;
    public $experienciaProgramador;

    public function __construct()
    {
        // Para heredar los valores del constructor padre se agrega lo siguiente:
        // parent::__construct();
        parent::__construct();
        $this->lenguajes = "HTML, CSS, PHP, MYSQL, etc...";
        $this->experienciaProgramador = 10;
    }

    public function sabeLenguajes($lenguajes)
    {
        $this->lenguajes = $lenguajes;

        return $this->lenguajes;
    }
    public function programar()
    {
        return "Soy programador";
    }
    public function repararPC()
    {
        return "Reparo computadoras";
    }
    public function offimatica()
    {
        return "Estoy escribiendo en Word";
    }
}

class TecnicoRedes extends informatico
{
    public $auditarRedes;
    public $experienciaRedes;

    public function __construct()
    {
        parent::__construct();
        $this->auditarRedes = "Experto";
        $this->experienciaRedes = 5;
    }

    public function Auditar()
    {
        return "Estoy auditando una red";
    }
}
