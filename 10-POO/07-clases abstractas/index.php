<?php
// Para declarar una clase abstracta se agrega abstract al inicio de la clase.
abstract class ordenador
{
    public $encendido;
    // declarar metodos abstractos cuando no se sabe que funcionalidades tendra en la clase hija.
    abstract public function encender();

    public function apagar()
    {
        $this->encendido = false;
    }
}

class pcAsus extends ordenador
{
    public $software;

    public function ArrancarSoftware()
    {
        $this->software = true;
    }
    /* 
    Se debe declarar el metodo encender de la clase Padre para que no nos de un error,
    es decir; debemos darle funcionalidad al metodo encender.
    */
    public function encender()
    {
        $this->encendido = true;
    }
}

$pc = new pcAsus;

$pc->ArrancarSoftware();
$pc->encender();
$pc->apagar();

var_dump($pc);
