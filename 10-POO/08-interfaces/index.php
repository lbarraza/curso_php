<?php
// Una interface sirve para indicar los metodos que debera tener la clase
interface pc
{
    public function encender();
    public function apagar();
    public function reiniciar();
    public function desfragmentar();
    public function detectarUsb();
}

// Para implementar una interface se debe añadir lo siguiente:
// class nombreClase implements Interface

class iMac implements pc
{
    private $modelo;

    function getModelo()
    {
        return $this->modelo;
    }

    function setModelo($modelo)
    {
        $this->modelo = $modelo;
    }

    public function encender()
    {
        return "Lo que sea";
    }
    public function apagar()
    {
        return "Lo que sea";
    }
    public function reiniciar()
    {
        return "Lo que sea";
    }
    public function desfragmentar()
    {
        return "Lo que sea";
    }
    public function detectarUsb()
    {
        return "Lo que sea";
    }
}

$pc = new iMac();
$pc->setModelo('Macbook PRO 2021');

echo $pc->getModelo();
var_dump($pc);
