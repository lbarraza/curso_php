<?php

class categoria
{
    public $nombre;

    public $descripcion;

    public function __construct()
    {
        $this->nombre = 'Video Juegos';
        $this->descripcion = 'Categoria enfocada en los video juegos';
    }

    function getNombre()
    {
        return $this->nombre;
    }

    function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    function getDescripcion()
    {
        return $this->descripcion;
    }

    function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }
}
