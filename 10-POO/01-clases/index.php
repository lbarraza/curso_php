<?php
// Programacion orientada a objetos
/*
* Es un paradigma de programacion (Una forma de programar) que nos permite estructurar nuestro codigo y aplicar buenas practicas,
* mediante la creación de objetos y la aplicación de funcionalidades a estos objetos.
*
*/

// Definir una clase (Molde para crear mas objetos de tipo auto con caracteristicas parecidas)

class Auto {
    // atributos o propiedades (Variables)
    public $color = "Rojo";
    public $marca = "Nissan";
    public $modelo = "Sentra";
    public $velocidad = 180;
    public $caballos_fuerza = 450;
    public $array_ruedas = array(4, 4, 4, 4);

    // Metodos, son acciones que hace el objeto (funciones)
    public function getColor(){
        // Busca en esta clase la propiedad "X"
        return $this->color;
    }
    
    public function setColor($color)
    {
        $this->color = $color;
    }

    public function acelerar()
    {
        $this->velocidad++;
    }

    public function frenar()
    {
        $this->velocidad--;
    }

    public function getVelocidad()
    {
        return $this->velocidad;
    }
    
    public function setModelo($modelo)
    {
        $this->modelo = $modelo;
    }
    public function getArray_ruedas() { 
        return $this->array_ruedas; 
   } 

   public function setArray_ruedas(array $ruedas) { 
        $this->array_ruedas = $ruedas;
   } 

} // fin de la clase

// Crear un objeto / instanciar la clase

$auto = new Auto();

// Usar los metodos
echo 'la velocidad es :' . $auto->getVelocidad();
echo "<br>";

echo "El color del auto es: ". $auto->getColor();
echo "<br>";
$auto->setColor('Negro');
echo "El color del auto ahora es: ". $auto->getColor();
echo "<br>";

$auto->acelerar();
$auto->acelerar();
$auto->acelerar();
$auto->acelerar();
$auto->frenar();
echo "La velocidad del auto es: ". $auto->getVelocidad();

$auto2 = new Auto();
$auto2->setColor('Naranja');
$auto2->setModelo('Versa');
echo "<br>";
// echo $auto->getArray_ruedas();

var_dump($auto);
var_dump($auto2);