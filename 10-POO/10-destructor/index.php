<?php

class user
{
    public $nombre;
    public $email;

    public function __construct()
    {
        $this->nombre = 'Luis';
        $this->email = 'luisfdopoker@hotmail.com';

        echo "Creando el objeto <br>";
    }
    public function __toString()
    {   
        return "hola, {$this->nombre}, estas registrado con {$this->email} <br>";
    }
    public function __destruct()
    {
        echo "Destruyendo el objeto";
    }
}

$user = new user();
echo $user;
// for ($i=0; $i <= 10; $i++) { 
//     echo $i . "<br>";
// }
// echo $user->nombre . "<br>";
