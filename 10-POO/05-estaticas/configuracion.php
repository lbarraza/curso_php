<?php

class Configuracion
{
	// las propiedaddes estaticas se definen de la siguiente forma:
	// public static $propiedad;
	public static $color;
	public static $newsletter;
	public static $entorno;

	/* 
	Los getters y setter estaticas se definen de la siguiente forma:
	public static function get/setNombre()
	{

	}
	*/
	public static function getColor()
	{
		// No se usa el $this-> en su lugar se utiliza el self::
		return self::$color;
	}

	public static function setColor($color)
	{
		self::$color = $color;
	}

	public static function getNewsletter()
	{
		return self::$newsletter;
	}

	public static function setNewsletter($newsletter)
	{
		self::$newsletter = $newsletter;
	}

	public static function getEntorno()
	{
		return self::$entorno;
	}

	public static function setEntorno($entorno)
	{
		self::$entorno = $entorno;
	}
}
