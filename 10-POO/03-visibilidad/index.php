<?php
require_once 'auto.php';

$auto =  new auto('Rojo', 'Ferrari', 'Demonico', 600, 1200, 2);

// incorrecto sin acceso a la propiedad.
// $auto->marca = 'Nissan';

// acceso a la propiedad protegida desde un metodo
$auto->setMarca('Nissan');
var_dump($auto);

// Acceder a una propiedad privada conm un metodo.
// $auto->modelo = 'error';
// print_r($auto->getModelo());
// echo "<br/>";
// $auto->private = 'naranja';
// print_r($auto->color);
// Si no establecemos o indicamos que el dato enviado al metodo es de tipo objeto,
// podremos enviar cualquier dato.
// En este caso dara error porque indicamos que debe recibir un objeto
echo $auto->mostrarInformacion('hola');

?>