<?php
// Programacion orientada a objetos


// Definir una clase (Molde para crear mas objetos de tipo coche con caracteristicas parecidas)

class Auto
{
    // atributos o propiedades (Variables)

    /* 
    VISIBILIDAD 
    PUBLIC: Podemos acceder desde cualquier lugar, dentro de la clase actual, dentro de las clases que
            hereden esta clase o fuera de la clase.
    PROTECTED: Se pueden acceder a ellos desde la clase que los define, y desde clases que hereden esta
            clase.
    PRIVATE: Unicamente se pueden acceder desde la clase.
    */
    public $color;
    protected $marca;
    private $modelo;
    public $velocidad;
    public $caballos_fuerza;


    public function __construct($color, $marca, $modelo, $velocidad, $caballos_fuerza, $plazas)
    {
        $this->color = $color;
        $this->marca = $marca;
        $this->modelo = $modelo;
        $this->velocidad = $velocidad;
        $this->caballos_fuerza = $caballos_fuerza;
        $this->plazas = $plazas;
    }

    // Metodos, son acciones que hace el objeto (funciones)
    public function getColor()
    {
        // Busca en esta clase la propiedad "X"
        return $this->color;
    }

    public function setColor($color)
    {
        $this->color = $color;
    }

    public function acelerar()
    {
        $this->velocidad++;
    }

    public function frenar()
    {
        $this->velocidad--;
    }
    public function getVelocidad()
    {
        return $this->velocidad;
    }
    public function setModelo($modelo)
    {
        $this->modelo = $modelo;
    }
    public function setMarca($marca)
    {
        $this->marca = $marca;
    }
    public function getModelo()
    {
        // Busca en esta clase la propiedad "X"
        return $this->modelo;
    }
    // Si no establecemos o indicamos que el dato enviado al metodo es de tipo objeto,
    // podremos enviar cualquier dato.
    public function mostrarInformacion(auto $miObjeto)
    {
        if (is_object($miObjeto)) {
            $informacion  = "<h1>La información del objeto es:</h1>";
            $informacion .= "Marca:".$miObjeto->marca;
            $informacion .= "<br/>Modelo:".$miObjeto->modelo;
            $informacion .= "<br/>Velocidad:".$miObjeto->velocidad;
        }else {
            $informacion = "<h1>Tu informacion es la siguiente:</h1>".$miObjeto;
        }
        return $informacion;
    }
} // fin de la clase



