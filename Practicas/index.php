<?php

/*
* 
* Practica 1
* Crear una variable por cada tipo de dato permitido en PHP e imprimirla por pantalla.
* Agregar un comentario señalando el tipo de dato que se utiliza.
*
*/

// Escribir aqui la practica 1

/*******************************************************************************************/

/*
* 
* Practica 2
* Crear un script que imprima por pantalla todos los números pares del 1 al 100.
* Agregar una explicación al código (Comentarios) que indique el proceso.
* Utilizar bucle While o For.
*/

// Escribir aqui la practica 2

/*******************************************************************************************/

/*
* 
* Practica 3
* Crear un script que imprima los cuadrados (un número multiplicado por si mismo) de 
* los primeros 50 números.
* 1.- Utilizar bucle While o For.
* 2.- Agregar una explicación al código (Comentarios) que indique el proceso.
* 
*/

// Escribir aqui la practica 3

/*******************************************************************************************/

/*
* 
* Practica 4
* Crear un script que obtenga por la el metodo GET el valor de 2 variables y realizar
* todas las operaciones aritméticas basicas que se pueden realizar con ellas.
* 1.- Comprobar que se han recibido los valores por GET. En caso contrario, mostrar un 
* mensaje de error.
* 2.- Agregar una explicación al código (Comentarios) que indique el proceso.
*
*/

// Escribir aqui la practica 4

/*******************************************************************************************/

/*
* 
* Practica 5
* Crear un script que imprima por pantalla todos los números que hay entre dos números
* que se reciben por el metodo POST
* 1.- Comprobar que se han recibido los valores por POST. En caso contrario, mostrar un
* mensaje de error.
* 2.- Validar que los valores sean numéricos. En caso contrario, mostrar un mensaje de error.
* 3.- Validar que el segundo número sea mayor que el primero.
* 4.- Agregar una explicación al código (Comentarios) que indique el proceso.
*
*/

// Escribir aqui la practica 5

/*******************************************************************************************/

/*
* 
* Practica 6
* Imprimir todas las tablas de multiplicar del 1 al 10. en una tabla de HTML.
* 1.- Agregar una explicación al código (Comentarios) que indique el proceso.
*
*/

// Escribir aqui la practica 6

/*******************************************************************************************/

/*
* 
* Practica 7
* Crear un script que imprima por pantalla todos los números que hay entre dos números
* que se reciben por el metodo POST, pero estos números se deben mostrar separados por
* numeros pares y por numeros impares.
* 1.- Comprobar que se han recibido los valores por POST. En caso contrario, mostrar un
* mensaje de error.
* 2.- Validar que los valores sean numéricos. En caso contrario, mostrar un mensaje de error.
* 3.- Validar que el segundo número sea mayor que el primero.
* 4.- Agregar una explicación al código (Comentarios) que indique el proceso.
*
*/

// Escribir aqui la practica 7

/*******************************************************************************************/

/*
* 
* Practica 8
* Considera estás desarrollando una web donde trabajas con tipos de motor (suponemos que se 
* trata del tipo de motor de una bomba para mover fluidos). Define una variable $tipoMotor y 
* asígnale un valor. Los valores posibles son 1, 2, 3, 4. A través de un condicional switch haz 
* lo siguiente:
* 
* a) Si el tipo de motor es 0, mostrar un mensaje indicando “No hay establecido un valor definido para el tipo de bomba”.
* b) Si el tipo de motor es 1, mostrar un mensaje indicando “La bomba es una bomba de agua”.
* c) Si el tipo de motor es 2, mostrar un mensaje indicando “La bomba es una bomba de gasolina”.
* d) Si el tipo de motor es 3, mostrar un mensaje indicando “La bomba es una bomba de hormigón”.
* e) Si el tipo de motor es 4,mostrar un mensaje indicando “La bomba es una bomba de pasta alimenticia”.
* f) Si no se cumple ninguno de los valores anteriores mostrar el mensaje “No existe un valor válido para tipo de bomba”.
*
*/

// Escribir aqui la practica 8

/*******************************************************************************************/

/*
* 
* Practica 9
* Crear un script que imprima por pantalla el contenido de un array de 10 elementos.
* 1.- Crear un array de 10 elementos (cualquier tipo de valores).
* 2.- Agregar una explicación al código (Comentarios) que indique el proceso.
* for y el foreach
*/

// Escribir aqui la practica 9

/*******************************************************************************************/

/*
* 
* Practica 10
* Crear un script que imprima por pantalla el contenido de un array asociativo de 10 elementos.
* 1.- Crear un array asociativo de 10 elementos (cualquier tipo de valores).
* 2.- Agregar una explicación al código (Comentarios) que indique el proceso.
*/

// Escribir aqui la practica 10

/*******************************************************************************************/

/*
* 
* Practica 11
* Crear un script que imprima por pantalla el contenido de un array multidimensional de 4x4 elementos.
* 1.- Crear un array multidimensional de 4x4 elementos (cualquier tipo de valores).
* 2.- Agregar una explicación al código (Comentarios) que indique el proceso.
* 3.- los datos de cada uno de los niveles
*/

// Escribir aqui la practica 11


?>