<!-- Hecho por Miguel Guel Gracia al 23/05/2022 -->
<html>
    <body>
<?php

/*
* 
* Practica 1
* Crear una variable por cada tipo de dato permitido en PHP e imprimirla por pantalla.
* Agregar un comentario señalando el tipo de dato que se utiliza.
*
*/

// Escribir aqui la practica 1
echo "Práctica 1";
echo "<br>";
{
    $int = 40; //Integer
    echo $int . " (Entero)";
    echo "<br>";
    $float = 5.5; //Float
    echo $float . " (Float)";
    echo "<br>";
    $string = "Hola mundo"; //String (cadena de texto)
    echo $string . " (String)";
    echo "<br>";
    $boolean = true; //Boolean
    echo $boolean . " (Booleano true)";
    echo "<br>";
    $null = NULL; //Null
    echo $null . " (Nulo)";
    echo "<br>";
    $array = array(1,2,3);//Array
    print_r($array);
    echo "<br>";
    //Objeto
    class Objeto {
        public $numero;
        public function __construct($numero){
            $this->numero=$numero;
        }

    }
    $objeto = new Objeto(5);
    print_r($objeto);
    echo "<br>";
}
echo "<br>";




/*******************************************************************************************/

/*
* 
* Practica 2
* Crear un script que imprima por pantalla todos los números pares del 1 al 100.
* Agregar una explicación al código (Comentarios) que indique el proceso.
* Utilizar bucle While o For.
*/

// Escribir aqui la practica 2
echo "Práctica 2" . "<br>";
{
    for ($i=1; $i <= 100; $i++) { //Se hace un for para recorrer los numeros del 1 al 100.
        if ($i%2==0) {            //Condicional, se checa si la división del numero actual entre 2 da un residuo de 0, de ser así, es un número par, así que se imprime el numero.
            echo $i . ", ";
        }
    }
}
echo "<br><br>";


/*******************************************************************************************/

/*
* 
* Practica 3
* Crear un script que imprima los cuadrados (un número multiplicado por si mismo) de 
* los primeros 50 números.
* 1.- Utilizar bucle While o For.
* 2.- Agregar una explicación al código (Comentarios) que indique el proceso.
* 
*/

// Escribir aqui la practica 3
echo "Práctica 3" . "<br>";
{
    for ($i=1; $i <=50 ; $i++) { //Se crea un for para recorrer los números del 1 al 50.
        echo $i*$i . ",";        //Se imprime el cuadrado del número.
    }
}
echo "<br>";
?>
<h1>Práctica 4</h1> <!-- HTML para mandar GETs de la misma página al php de la práctica. Contiene dos campos de entrada y un botón para mandar los parametros. -->
    <form method="GET">
    <label for="numero1">Numero 1</label>
    <input type="number" name="num1" />
    <label for="numero2">Numero 2</label>
    <input type="number" name="num2" />
    <input type="submit" value="Calcular"/>
    </form>

<?php
/*******************************************************************************************/

/*
* 
* Practica 4
* Crear un script que obtenga por la el metodo GET el valor de 2 variables y realizar
* todas las operaciones aritméticas basicas que se pueden realizar con ellas.
* 1.- Comprobar que se han recibido los valores por GET. En caso contrario, mostrar un 
* mensaje de error.
* 2.- Agregar una explicación al código (Comentarios) que indique el proceso.
*
*/

// Escribir aqui la practica 4
if (!(empty($_GET["num1"])||empty($_GET["num2"]))) { //Se checa que los parametros no esten vacíos al ser enviados para no cometer un error, de estar vacíos, no se hace nada.
    $num1 = $_GET["num1"]; //Se asignan los valores de cada parametro a las variables.
    $num2 = $_GET["num2"];
    //Se realizan las operaciones.
    echo $num1 . " + " . $num2 . " = ";
    echo $num1+$num2;
    echo "<br>";
    echo $num1 . " - " . $num2 . " = ";
    echo $num1-$num2;
    echo "<br>";
    echo $num1 . " * " . $num2 . " = ";
    echo $num1*$num2;
    echo "<br>";
    echo $num1 . " / " . $num2 . " = ";
    echo $num1/$num2;
    echo "<br>";

}
?>
<h1>Práctica 5</h1> <!--HTML para dar entradas tipo post al php, contiene dos campos y un boton para enviar los datos como POST al mismo php -->
    <form method="POST">
    <label for="numero1">Numero 1</label>
    <input type="number" name="limit1" />
    <label for="numero2">Numero 2</label>
    <input type="number" name="limit2" />
    <input type="submit" value="Imprimir"/>
    </form>
<?php
/*******************************************************************************************/

/*
* 
* Practica 5
* Crear un script que imprima por pantalla todos los números que hay entre dos números
* que se reciben por el metodo POST
* 1.- Comprobar que se han recibido los valores por POST. En caso contrario, mostrar un
* mensaje de error.
* 2.- Validar que los valores sean numéricos. En caso contrario, mostrar un mensaje de error.
* 3.- Validar que el segundo número sea mayor que el primero.
* 4.- Agregar una explicación al código (Comentarios) que indique el proceso.
*
*/

// Escribir aqui la practica 5
if (!(empty($_POST["limit1"])||empty($_POST["limit2"]))) { //Se checa que los parametros no esten vacíos al ser enviados para no cometer un error, de estar vacíos, no se hace nada.
    $num1=$_POST["limit1"];
    $num2=$_POST["limit2"];
    if ($num2>$num1){ //Se verifica que el segundo número recibido sea mayor al primero, de ser así, entra al for.
        for ($i=$num1+1; $i<$num2  ; $i++) { //Se recorren los números entre ambos números recibidos, y se imprimen SOLAMENTE los valores entre los dos números, sin incluir a ambos.
            echo $i . ", ";
        }
    }

}
/*******************************************************************************************/
?>
<h1>Práctica 6</h1>
<?php
/*
* 
* Practica 6
* Imprimir todas las tablas de multiplicar del 1 al 10. en una tabla de HTML.
* 1.- Agregar una explicación al código (Comentarios) que indique el proceso.
*
*/

// Escribir aqui la practica 6
echo "<table>"; //Se abre la tabla.
for ($i=1; $i<11; $i++){ //Con este for, se crearan las filas de la tabla.
    echo "<tr>"; //Se abre una fila de la tabla.
    for ($j=1; $j<11; $j++){//Con este for, se crearan las celdas de cada fila de la tabla.
        $resultado=$j*$i; //Se consigue el resultado de la multiplicación del contador de cada ambos for.
        echo "<td>$j*$i = $resultado</td>"; //Se abre una celda, se inserta el resultado de la multiplicación y se cierra la celda.

    }
    echo "</tr>"; //Despues de crear cada multiplicación por 1 (1x1, 2x1, 3x1, etc), se cierra la fila para seguir con el siguiente número.
}
echo "</table>";//Se cierra la tabla para poder desplegarse.

/*******************************************************************************************/
?>
<h1>Práctica 7</h1> <!--HTML para crear 2 campos y un botón de imprimir para mandar un POST a la misma página-->
    <form method="POST">
    <label for="numero1">Numero 1</label>
    <input type="number" name="limite1" />
    <label for="numero2">Numero 2</label>
    <input type="number" name="limite2" />
    <input type="submit" value="Imprimir"/>
    </form>
<?php
/*
* 
* Practica 7
* Crear un script que imprima por pantalla todos los números que hay entre dos números
* que se reciben por el metodo POST, pero estos números se deben mostrar separados por
* numeros pares y por numeros impares.
* 1.- Comprobar que se han recibido los valores por POST. En caso contrario, mostrar un
* mensaje de error.
* 2.- Validar que los valores sean numéricos. En caso contrario, mostrar un mensaje de error.
* 3.- Validar que el segundo número sea mayor que el primero.
* 4.- Agregar una explicación al código (Comentarios) que indique el proceso.
*
*/

// Escribir aqui la practica 7
    if (!(empty($_POST["limite1"])||empty($_POST["limite2"]))) { //Se checa que los parametros no esten vacíos al ser enviados para no cometer un error, de estar vacíos, no se hace nada.
        $num1=$_POST["limite1"];
        $num2=$_POST["limite2"];
        $pares = [];
        $impares = [];
        if ($num2>$num1){ //Se verifica que el segundo número recibido sea mayor al primero, de ser así, entra al for.
            for ($i=$num1+1; $i<$num2  ; $i++) { //Se recorren los números entre ambos números recibidos, y se imprimen SOLAMENTE los valores entre los dos números, sin incluir a ambos.
                if ($i%2==0){//Se verifica si el numero actual es par mediante el residuo por la división de 2
                    array_push($pares, $i); //Si lo es, se agrega al array de pares.
                }
                else {
                    array_push($impares,$i); //Si no lo es, se agrega al array de impares.
                }
            }
        }
        //Se imprimen ambos arrays mediante un for.
        for ($i=0; $i <sizeof($pares) ; $i++) { 
            echo $pares[$i] . ", ";
        }
        echo "<br>";
        for ($i=0; $i <sizeof($impares) ; $i++) { 
            echo $impares[$i] . ", ";
        }
        echo "<br><br>";

    }
?>
<?php
/*******************************************************************************************/

/*
* 
* Practica 8
* Considera estás desarrollando una web donde trabajas con tipos de motor (suponemos que se 
* trata del tipo de motor de una bomba para mover fluidos). Define una variable $tipoMotor y 
* asígnale valor 3. Los valores posibles son 1, 2, 3, 4. A través de un condicional switch haz 
* lo siguiente:
* 
* a) Si el tipo de motor es 0, mostrar un mensaje indicando “No hay establecido un valor definido para el tipo de bomba”.
* b) Si el tipo de motor es 1, mostrar un mensaje indicando “La bomba es una bomba de agua”.
* c) Si el tipo de motor es 2, mostrar un mensaje indicando “La bomba es una bomba de gasolina”.
* d) Si el tipo de motor es 3, mostrar un mensaje indicando “La bomba es una bomba de hormigón”.
* e) Si el tipo de motor es 4,mostrar un mensaje indicando “La bomba es una bomba de pasta alimenticia”.
* f) Si no se cumple ninguno de los valores anteriores mostrar el mensaje “No existe un valor válido para tipo de bomba”.
*
*/

// Escribir aqui la practica 8
    echo "Práctica 8" . "<br>";
    $tipoMotor=3;
    switch ($tipoMotor) { //Se crea un switch para la variable $tipoMotor, cada caso mostrando un mensaje distinto. Por los requisitos de la práctica, el caso a tratar siempre sera
                          //el '3'.
        case '0':
            echo "No hay establecido un valor definido para el tipo de bomba." . "<br>";
            break;
        case '1':
            echo "La bomba es una bomba de agua." . "<br>";
            break;
        case '2':
            echo "La bomba es una bomba de gasolina." . "<br>";
            break;
        case '3':
            echo "La bomba es una bomba de hormigón." . "<br>";
            break;
        case '4':
            echo "La bomba es una bomba de pasta alimenticia." . "<br>";
            break;
        default:
            break;
    }
    echo "<br><br>";
?>
<?php
/*******************************************************************************************/
/*
* 
* Practica 9
* Crear un script que imprima por pantalla el contenido de un array de 10 elementos.
* 1.- Crear un array de 10 elementos (cualquier tipo de valores).
* 2.- Agregar una explicación al código (Comentarios) que indique el proceso.
*/

// Escribir aqui la practica 9
    echo "Práctica 9" . "<br>";
    $arreglo=[0,1,2,3,4,5,6,7,8,9]; //Se crea un array con 10 números
    for ($i=0; $i < sizeof($arreglo) ; $i++) { //Se hace un for que recorre el arreglo, imprimiendo uno de los elementos en cada ciclo.
        echo $arreglo[$i] . ", ";
    }
    echo "<br><br>";
?>
<?php
/*******************************************************************************************/

/*
* 
* Practica 10
* Crear un script que imprima por pantalla el contenido de un array asociativo de 10 elementos.
* 1.- Crear un array asociativo de 10 elementos (cualquier tipo de valores).
* 2.- Agregar una explicación al código (Comentarios) que indique el proceso.
*/

// Escribir aqui la practica 10
    echo "Práctica 10"."<br>";
    $arreglo = array( //Se crea el array asociativo, nombres de números y números.
        'Cero' => 0,
        'Uno' => 1,
        'Dos' => 2,
        'Tres' => 3,
        'Cuatro' => 4,
        'Cinco' => 5,
        'Seis' => 6,
        'Siete' => 7,
        'Ocho' => 8,
        'Nueve' => 9,
    );
    foreach ($arreglo as $key => $value) { //Se recorre cada registro y valor del array asociativo, imprimiendo ambos, donde $key es el registro y $value su valor.
        echo $key . " es " . $value . "<br>";
    }
    echo "<br>";
?>
<?php
/*******************************************************************************************/

/*
* 
* Practica 11
* Crear un script que imprima por pantalla el contenido de un array multidimensional de 4x4 elementos.
* 1.- Crear un array multidimensional de 4x4 elementos (cualquier tipo de valores).
* 2.- Agregar una explicación al código (Comentarios) que indique el proceso.
*/

// Escribir aqui la practica 11
    echo "Práctica 11" . "<br>";
    $arregloM= array( //Se crea un arreglo de arreglos.
        array(0,1,2,3),
        array('Cero','Uno','Dos','Tres'),
        array('Jorge','Luis','Carlos','Manuel'),
        array(true,false,false,true)
    );
    for ($i=0; $i < sizeof($arregloM) ; $i++) { //Con este for, se recorre cada elemento del arreglo principal y se imprime de que arreglo se va a imprimir elementos.
        echo "Arreglo " . $i . ": ";
        for ($j=0; $j < sizeof($arregloM[$i]); $j++){//Con este for, se recorre cada elemente del arreglo secundario y se imprime sus elementos.
            echo $arregloM[$i][$j] . ", "; //Se imprime el elemento $j del arreglo $i.
        }
        echo "<br>";
    }
?>
</body>
</html>