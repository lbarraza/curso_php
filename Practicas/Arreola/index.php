<?php
echo '<hr>';
echo "<h3> Práctica  1 <h3>";

/*
* 
* Practica 1
* Crear una variable por cada tipo de dato permitido en PHP e imprimirla por pantalla.
* Agregar un comentario señalando el tipo de dato que se utiliza.
*
*/

$int = 77;
echo $int . "<br>";
echo "Este valor es de tipo ";
echo gettype($int)."<br>";
echo "<br>";

$float  = 3.14;
echo $float."<br>";
echo "Este valor es de tipo ";
echo gettype($float)."<br>";
echo "<br>";

$string = "gato";
echo $string."<br>";
echo "Este valor es de tipo ";
echo gettype($string)."<br>";
echo "<br>";

$bool = true;
echo $bool."<br>";
echo "Este valor es de tipo ";
echo gettype($bool)."<br>";
echo "<br>";

$array = [
    'uno',
    'dos',
    'tres'
];
echo "<br>";
var_dump($array);
echo "<br>";
echo "Este valor es de tipo ";
echo gettype($array)."<br>";


/*******************************************************************************************/
echo '<hr>';
echo "<h3> Práctica  2 <h3>";
echo "Números pares <br>";

$num=1;
while ($num <= 100) { //condición
    $resultado = ($num % 2) == 0 ? $resultado = $num : false; //Si el residuo es cero, imprimir
    $num+=1;
    echo $resultado." ";
} 
/*
* 
* Practica 2
* Crear un script que imprima por pantalla todos los números pares del 1 al 100.
* Agregar una explicación al código (Comentarios) que indique el proceso.
* Utilizar bucle While o For.
*/

// Escribir aqui la practica 2

/*******************************************************************************************/
echo '<hr>';
echo "<h3> Práctica  3 <h3>";
echo "Primeros 50 números al cuadrado <br>";
for ($i=1; $i <= 50; $i++) { //mientras sea menor a 51
    echo $i * $i . " ";  // imprime número al cuadrado
}
/*
* 
* Practica 3
* Crear un script que imprima los cuadrados (un número multiplicado por si mismo) de 
* los primeros 50 números.
* 1.- Utilizar bucle While o For.
* 2.- Agregar una explicación al código (Comentarios) que indique el proceso.
* 
*/

// Escribir aqui la practica 3

/*******************************************************************************************/
echo '<hr>';
echo "<h3> Práctica  4 <h3>";
echo "Dos variables por get<br>";

//Valores GEt recibidos?
if (isset($_GET['num1']) && isset($_GET['num2']) ) { 
    //Suma
    echo "<br> Suma <br>";
    echo $_GET['num1'] . " + " . $_GET['num2'] . " = ";
    echo (int)$_GET['num1'] + (int)$_GET['num2'];
    echo "<br>";
    
    //Resta
    echo "<br> Resta <br>";
    echo ($_GET['num1'])." - ". $_GET['num2']." = ";
    echo (int)$_GET['num1'] - (int)$_GET['num2'];
    echo "<br>";
    
    //División
    echo "<br> Divión <br>";
    echo $_GET['num1']." / ". $_GET['num2']." = ";
    echo (int)$_GET['num1'] / (int)$_GET['num2'];
    echo "<br>";
    
    //Multiplicación
    echo "<br> Multiplicación <br>";
    echo $_GET['num1']." x ". $_GET['num2']." = ";
    echo (int)$_GET['num1'] * (int)$_GET['num2'];
    echo "<br>";
}else {
    echo "Error al recibir un valor GET <br>";
}

//var_dump($_GET);
/*
* 
* Practica 4
* Crear un script que obtenga por la el metodo GET el valor de 2 variables y realizar
* todas las operaciones aritméticas basicas que se pueden realizar con ellas.
* 1.- Comprobar que se han recibido los valores por GET. En caso contrario, mostrar un 
* mensaje de error.
* 2.- Agregar una explicación al código (Comentarios) que indique el proceso.
*
*/

// Escribir aqui la practica 4

/*******************************************************************************************/
echo '<hr>';
echo "<h3> Práctica 5 <h3>";
echo "Los números son: <br>";
if (isset($_POST['num1']) && isset($_POST['num2']) ) {  //validar recibir datos de POST
    if ((int)$_POST['num1'] == $_POST['num1']) { //validar que  sean números
        if($_POST['num1']  < $_POST['num2']){ //validar que  el primero sea menor
            $i = $_POST['num1']+1;
            for ($i; $i < $_POST['num2']; $i++) {  //imprime los números entre los valores
                echo $i."<br>";
            }
        }else{
            echo "El primer número es más grande que el segundo <br>";
        }
    }else{
        echo "Un valor no es un número <br>";
    }
}else{
    echo "Error al recibir un valor POST <br>";
}

/*
* 
* Practica 5
* Crear un script que imprima por pantalla todos los números que hay entre dos números
* que se reciben por el metodo POST
* 1.- Comprobar que se han recibido los valores por POST. En caso contrario, mostrar un
* mensaje de error.
* 2.- Validar que los valores sean numéricos. En caso contrario, mostrar un mensaje de error.
* 3.- Validar que el segundo número sea mayor que el primero.
* 4.- Agregar una explicación al código (Comentarios) que indique el proceso.
*
*/

// Escribir aqui la practica 5

/*******************************************************************************************/

echo '<hr>';
echo "<h3> Práctica 6 <h3>";
echo "<table border='1'>"; //crear tabla con borde
for ($i=1; $i <= 10; $i++) {  //crear cuadro por valores verticales
    echo '<tr>'; 
    for ($e=1; $e <= 10; $e++) { //crear cuadro por valores horizontales
        echo "<td>".$i." x ".$e. "</td>";
        echo "<td bgcolor='red'>".$i*$e."</td>";
    }
    echo '</tr>';
}
echo "</table>";
/*
* 
* Practica 6
* Imprimir todas las tablas de multiplicar del 1 al 10. en una tabla de HTML.
* 1.- Agregar una explicación al código (Comentarios) que indique el proceso.
*
*/

// Escribir aqui la practica 6

/*******************************************************************************************/
echo '<hr>';
echo "<h3> Práctica 7 <h3>";
echo "Los números son: <br>";
$pares = array();
$impares = array();
if (isset($_POST['num1']) && isset($_POST['num2']) ) {  //validar recibir datos de POST
    if ((int)$_POST['num1'] == $_POST['num1']) { //validar que  sean números
        if($_POST['num1']  < $_POST['num2']){ //validar que  el primero sea menor
            $i = $_POST['num1']+1;
            for ($i; $i < $_POST['num2']; $i++) {  //imprime los números entre los valores
                if ($i % 2 == 0) {
                    array_push($pares, $i); //añadir a array par
                }else{
                    array_push($impares, $i); //añadir array impar
                }
                
            }
        }else{
            echo "El primer número es más grande que el segundo <br>";
        }
    }else{
        echo "Un valor no es un número <br>";
    }
}else{
    echo "Error al recibir un valor POST <br>";
}

echo "Numeros pares son <br>"; //imprimir resultados array par
for ($i = 0; $i < count($pares); $i++) {
    echo $pares[$i]." ";
}
echo "<br>";

echo "Numeros impares son <br>"; //imprimir resultados array impar
for ($i = 0; $i < count($impares); $i++) {
    echo $impares[$i]." ";
}
echo "<br>";

/*
* 
* Practica 7
* Crear un script que imprima por pantalla todos los números que hay entre dos números
* que se reciben por el metodo POST, pero estos números se deben mostrar separados por
* numeros pares y por numeros impares.
* 1.- Comprobar que se han recibido los valores por POST. En caso contrario, mostrar un
* mensaje de error.
* 2.- Validar que los valores sean numéricos. En caso contrario, mostrar un mensaje de error.
* 3.- Validar que el segundo número sea mayor que el primero.
* 4.- Agregar una explicación al código (Comentarios) que indique el proceso.
*
*/

// Escribir aqui la practica 7

/*******************************************************************************************/
echo '<hr>';
echo "<h3> Práctica 8 <h3>";
$tipoMotor = 10;
echo "<br>";
switch ($tipoMotor){
    case 0:
        echo "No hay establecido un valor definido para el tipo de bomba";
    break;
    case 1:
        echo "La bomba es una bomba de agua";
    break;
    case 2:
        echo "La bomba es una bomba de gasolina";
    break;
    case 3:
        echo "La bomba es una bomba de hormigón";
    break;
    case 4:
        echo "La bomba es una bomba de pasta alimenticia";
    default:
        echo "No existe un valor válido para tipo de bomba" ;
}

/*
* 
* Practica 8
* Considera estás desarrollando una web donde trabajas con tipos de motor (suponemos que se 
* trata del tipo de motor de una bomba para mover fluidos). Define una variable $tipoMotor y 
* asígnale valor 3. Los valores posibles son 1, 2, 3, 4. A través de un condicional switch haz 
* lo siguiente:
* 
* a) Si el tipo de motor es 0, mostrar un mensaje indicando “No hay establecido un valor definido para el tipo de bomba”.
* b) Si el tipo de motor es 1, mostrar un mensaje indicando “La bomba es una bomba de agua”.
* c) Si el tipo de motor es 2, mostrar un mensaje indicando “La bomba es una bomba de gasolina”.
* d) Si el tipo de motor es 3, mostrar un mensaje indicando “La bomba es una bomba de hormigón”.
* e) Si el tipo de motor es 4,mostrar un mensaje indicando “La bomba es una bomba de pasta alimenticia”.
* f) Si no se cumple ninguno de los valores anteriores mostrar el mensaje “No existe un valor válido para tipo de bomba”.
*
*/

// Escribir aqui la practica 8

/*******************************************************************************************/
echo '<hr>';
echo "<h3> Práctica 9 <h3>";
$numeros = array(0,1,2,3,4,5,6,7,8,9); 
for ($i=1; $i < 11 ; $i++) { //cre array del 1 al 10
   array_push($numeros,$i);
}
//var_dump($numeros);
foreach ($numeros as $key => $valor) { //imprime array
    echo $valor." ";

    }
    echo "<br> ";
    // var_dump($numeros);
/*
* 
* Practica 9
* Crear un script que imprima por pantalla el contenido de un array de 10 elementos.
* 1.- Crear un array de 10 elementos (cualquier tipo de valores).
* 2.- Agregar una explicación al código (Comentarios) que indique el proceso.
*/

// Escribir aqui la practica 9

/*******************************************************************************************/
echo '<hr>';
echo "<h3> Práctica 10 <h3>";
$conteo = array( //array asociativo
    'uno' => '1',
    'dos' => '2',
    'tres' => '3',
    'cuatro' => '4',
    'cinco' => '5',
    'seis' => '6',
    'siete' => '7',
    'ocho' => '8',
    'nueve' => '9',
    'diez' => '10'
);
foreach ($conteo as $key => $value) {//imprime array asociativo
   echo $key." es ".$value."<br> ";
}


/*
* 
* Practica 10
* Crear un script que imprima por pantalla el contenido de un array asociativo de 10 elementos.
* 1.- Crear un array asociativo de 10 elementos (cualquier tipo de valores).
* 2.- Agregar una explicación al código (Comentarios) que indique el proceso.
*/

// Escribir aqui la practica 10

/*******************************************************************************************/
echo '<hr>';
echo "<h3> Práctica 11 <h3>";

$arrayMultiple = [  //crea array múltiple
    'array1' => [
        'nombre' => 'Jose',
        'apellido' => 'Arreola',
        'número' => '7',
        'edad' => '10',
    ],
    'array2' => [
        'nombre' => 'Jorge',
        'email' => 'Perez',
        'número' => '6',
        'edad' => '24',
    ],
    'array3' => [
        'nombre' => 'David',
        'email' => 'Lopez',
        'número' => '32',
        'edad' => '56',
    ],
    'array4' => [
        'nombre' => 'Juan',
        'email' => 'Parra',
        'número' => '5',
        'edad' => '23',
    ], 
];
foreach ($arrayMultiple as $key => $value) { //imprimir array múltiple
    foreach ($value as $key2 => $value2) {
        echo $key2." = ".$value2."<br>";
    }
    echo "<br>";
    //var_dump($value)
}

/*
* 
* Practica 11
* Crear un script que imprima por pantalla el contenido de un array multidimensional de 4x4 elementos.
* 1.- Crear un array multidimensional de 4x4 elementos (cualquier tipo de valores).
* 2.- Agregar una explicación al código (Comentarios) que indique el proceso.
*/

// Escribir aqui la practica 11


?>