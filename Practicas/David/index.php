<?php


function Enter(){
    echo '<br>';
}

/*
* 
* Practica 1
* Crear una variable por cada tipo de dato permitido en PHP e imprimirla por pantalla.
* Agregar un comentario señalando el tipo de dato que se utiliza.
*
*/

// Escribir aqui la practica 1
echo "((Practica 1))";
echo "<br/>";
$var1=0.005;
$var2=10;
$var3='Hola Mundo';
$var4=true;
$var5=$_SERVER['SERVER_NAME'];
echo $var2; // Variable entera
echo '<br>';
echo $var1; // Variable flotante o decimal
echo '<br>';
echo $var3; // Variable Cadena de Texto
echo '<br>';
echo $var4; // Variable booleana
echo '<br>';
echo $var5; // Variable super global
echo '<br>';
echo '<br>';

/*******************************************************************************************/

/*
* 
* Practica 2
* Crear un script que imprima por pantalla todos los números pares del 1 al 100.
* Agregar una explicación al código (Comentarios) que indique el proceso.
* Utilizar bucle While o For.
*/

// Escribir aqui la practica 2

//Para el codigo aplicaremos el residuo 2

//Variante for
echo "((Practica 2))";
echo '<br/>';
echo "Numeros Pares del 1 al 100 (FOR)";
echo '<br>';
for($i = 0; $i < 100; $i++){ // observación: Aqui se pudo inicializar el contador en 1
    if($i%2 == 1){ // observación:  Aqui se puede utilizar el residuo 2 == 0, haciendo que solo se impriman los numeros pares sin sumar 1 al contador
        echo ($i+1) . " "; // Se imprimen los numeros pares, se agrega uno porque el for comienza en el cero
    }
    if($i%10==0){ // observación: Esto es una buena iniciativa
        echo '<br>'; // Se hace enter cada 10 para ahorrar espacio
    }
}
echo '<br/>';
echo '<br>';
//Variante while
echo "Numeros Pares del 1 al 100 (WHILE)";
echo '<br>';
// observación: Ejercicio correcto
$counter=1;

while($counter <= 100){
    if($counter%2 == 0){
        echo $counter." ";
    }
    if($counter%10==0){
        echo '<br>';
    }
    $counter++;
}
echo '<br>';
echo '<br>';


/*******************************************************************************************/

/*
* 
* Practica 3
* Crear un script que imprima los cuadrados (un número multiplicado por si mismo) de 
* los primeros 50 números.
* 1.- Utilizar bucle While o For.
* 2.- Agregar una explicación al código (Comentarios) que indique el proceso.
* 
*/

// Escribir aqui la practica 3

echo "((Practica 3))";
echo '<br/>';
echo "Numeros Cuadrados hasta el 50 (FOR)";
echo '<br>';
for($i=0;$i<50;$i++){ // observación: Aqui se pudo inicializar el contador en 1 y el condicional en <= 50, en este ejemplo unicamente llega a 49
        echo ($i*$i)." "; //Se imprimen los numeros cuadrados
    
    if($i%10==0){
        echo '<br>'; // Se hace enter cada 10 para ahorrar espacio
    }
}
echo '<br/>';
echo '<br>';
//Variante while
echo "Numeros Cuadrados hasta el 50 (WHILE)";
echo '<br>';
$counter=1;
// observación: Ejercicio correcto
while($counter<=50){
    echo ($counter*$counter)." "; //Se imprimen los numeros cuadrados
    if($counter%10==0){
        echo '<br>'; // Se hace enter cada 10 para ahorrar espacio
    }
    $counter++;
}




/*******************************************************************************************/

/*
* 
* Practica 4
* Crear un script que obtenga por la el metodo GET el valor de 2 variables y realizar
* todas las operaciones aritméticas basicas que se pueden realizar con ellas.
* 1.- Comprobar que se han recibido los valores por GET. En caso contrario, mostrar un 
* mensaje de error.
* 2.- Agregar una explicación al código (Comentarios) que indique el proceso.
*
*/

// Escribir aqui la practica 4
echo "<br>";
echo '((Practica 4))';
echo "<br>";
$n1=$_GET['N1']; // observación:  Aqui falto comprobar si los datos fueron recibidos por GET
$n2=$_GET['N2'];
// ejemplo
if (isset($_GET['N1']) && isset($_GET['N2'])){
    $n1=$_GET['N1'];
    $n2=$_GET['N2'];
}else{
    echo "Error: no se han recibido los datos por GET";
}
// el utilizar intval fue buena idea forzando a que los valores sean enteros
if(gettype( intval($n1))=="integer" & gettype(     intval($n2)     )=="integer"){
    //Verificamos que sean enteros los datos introducidos

    echo $n1+$n2;
    Enter(); // observación: Establecer esta funcion es una buena iniciativa, felicidades.
    echo $n1-$n2;
    Enter();
    echo $n1*$n2;
    Enter();
    echo $n1/$n2;
    Enter();
    echo $n1%$n2;
    Enter();
    echo ++$n1;
    Enter();
    echo $n1++;
    Enter();
    echo --$n1;
    Enter();
    echo $n1--;
    Enter();
    echo $n1+=3;
    Enter();
    echo $n1-=3;
    Enter();
    //Hacemos las operaciones
}else{
    echo "ERROR, verifique los datos";
    //Mensaje de Error
}


/*******************************************************************************************/

/*
* 
* Practica 5
* Crear un script que imprima por pantalla todos los números que hay entre dos números
* que se reciben por el metodo POST
* 1.- Comprobar que se han recibido los valores por POST. En caso contrario, mostrar un
* mensaje de error.
* 2.- Validar que los valores sean numéricos. En caso contrario, mostrar un mensaje de error.
* 3.- Validar que el segundo número sea mayor que el primero.
* 4.- Agregar una explicación al código (Comentarios) que indique el proceso.
*
*/

// Escribir aqui la practica 5
echo "<br>";
echo '((Practica 5))';
echo "<br>";

$n3=$_POST['N3']; // observación:  Aqui falto comprobar si los datos fueron recibidos por POST
$n4=$_POST['N4'];
echo $n4;
echo $n3;
echo "<br>";
if(gettype( intval($n3))=="integer" & gettype(     intval($n4)     )=="integer"){
    //Verificamos que sean enteros los datos introducidos
    if($n3<$n4){
        $a=$n3; // observación: Esto es incesesario, ya que $n3 tiene ese valor. Para que asignarlo a $a no es necesario
        // $n3 = $n3 + 1; // observación: Esto se pudo hacer para obtener el valor siguiente del primer numero
        // $n3++; // observación: Esto tambien se pudo hacer para obtener el valor siguiente del primer numero
        while($a<=$n4){ // observación: Aqui debio ser unicamente $a<$n4.
            echo $a." ";
            $a++;
        }

    }else{
        echo "ERROR: el primer número debe ser menor que el segundo";
    }
   
}else{
    echo "ERROR, verifique los datos";
    //Mensaje de Error
}


echo "<br>";
// var_dump($_POST);
/*******************************************************************************************/

/*
* 
* Practica 6
* Imprimir todas las tablas de multiplicar del 1 al 10. en una tabla de HTML.
* 1.- Agregar una explicación al código (Comentarios) que indique el proceso.
*
*/
// observación: Ejercicio incorrecto, te agrego un ejemplo para que lo analices

echo '<table border="1">'; // Se crea la tabla
echo '<tr>'; // Se crea la cabecera
for ($i=1; $i <= 10; $i++) { 
    echo '<td>tabla del ' . $i . '</td>';
}
echo '</tr>';
echo '<tr>'; // se crea la fila del contenido
for ($i=1; $i <= 10; $i++) { // iniciamos este for para que se repita 10 veces (numero de tablas)
    echo '<td>';
    for ($j=1; $j <= 10; $j++) {  // iniciamos este for para que se repita 10 veces (multiplicaciones por tabla)
        echo $i . 'X' . $j . '=' . $i*$j . '<br>';
        // var_dump($i, $j); // Descomentar para ver el valor de i y j en cada iteracion. Asi podemos ver el flujo de ejecucion
    }
    echo '</td>';
}
echo '</tr>';
echo '</table>'; // Se cierra la tabla


/*******************************************************************************************/

/*
* 
* Practica 7
* Crear un script que imprima por pantalla todos los números que hay entre dos números
* que se reciben por el metodo POST, pero estos números se deben mostrar separados por
* numeros pares y por numeros impares.
* 1.- Comprobar que se han recibido los valores por POST. En caso contrario, mostrar un
* mensaje de error.
* 2.- Validar que los valores sean numéricos. En caso contrario, mostrar un mensaje de error.
* 3.- Validar que el segundo número sea mayor que el primero.
* 4.- Agregar una explicación al código (Comentarios) que indique el proceso.
*
*/
// observación: Ejercicio incorrecto, te agrego un ejemplo para que lo analices
if(isset($_POST['N3']) && isset($_POST['N4'])) { // comprobamos que se han recibido los datos por POST
    if (is_numeric($_POST['N3']) && is_numeric($_POST['N4'])) { // comprobamos que los datos son numericos
        // si todo es correcto asignamos valores a las variables
        $numero1 = $_POST['N3']; // asignamos el valor del primer numero
        $numero2 = $_POST['N4']; // asignamos el valor del segundo numero
        if ($numero1 < $numero2) { // comprobamos que el primer numero sea menor que el segundo
            for ($i = $numero1; $i <= $numero2 ; $i++) { 
                if (($i % 2) == 0) {
                    $pares[] = $i; // si el numero es par lo guardamos en un array
                } else {
                    $impares[] = $i; // si el numero es impar lo guardamos en un array
                }
            }
            echo '<br>';
            echo '<h4>Numeros pares</h4>';
            foreach ($pares as $value) {
                echo $value . '<br>';
            }
            echo '<h4>Numeros impares</h4>';
            foreach ($impares as $value) {
                echo $value . '<br>';
            }
            echo '<br>';
        }else{
            // si no se cumple la condicion mostramos un mensaje de error
            echo "el numero 1 es mayor al numero 2";
        }
    }    
}else{
    // si no se cumple la condicion mostramos un mensaje de error
    echo "NO se han recibido los datos por POST";
}


/*******************************************************************************************/

/*
* 
* Practica 8
* Considera estás desarrollando una web donde trabajas con tipos de motor (suponemos que se 
* trata del tipo de motor de una bomba para mover fluidos). Define una variable $tipoMotor y 
* asígnale valor 3. Los valores posibles son 1, 2, 3, 4. A través de un condicional switch haz 
* lo siguiente:
* 
* a) Si el tipo de motor es 0, mostrar un mensaje indicando “No hay establecido un valor definido para el tipo de bomba”.
* b) Si el tipo de motor es 1, mostrar un mensaje indicando “La bomba es una bomba de agua”.
* c) Si el tipo de motor es 2, mostrar un mensaje indicando “La bomba es una bomba de gasolina”.
* d) Si el tipo de motor es 3, mostrar un mensaje indicando “La bomba es una bomba de hormigón”.
* e) Si el tipo de motor es 4,mostrar un mensaje indicando “La bomba es una bomba de pasta alimenticia”.
* f) Si no se cumple ninguno de los valores anteriores mostrar el mensaje “No existe un valor válido para tipo de bomba”.
*
*/

// Escribir aqui la practica 8

echo "((Practica 8)) Tipos de Motor";
echo "<br>";
$TypeMotor=3;

switch($TypeMotor){
    case 0:
        echo "No hay establecido un valor definido para el tipo de bomba";
        break;
    case 1:
        echo "La bomba es una bomba de agua";
        break;
    case 2:
        echo "La bomba es una bomba de gasolina";
        break;
    case 3:
        echo "La bomba es una bomba de hormigón";
        break;
    case 4:
        echo "La bomba es una bomba de pasta alimenticia";
         break;
    default:
        echo "No existe un valor válido para tipo de bomba";

}




/*******************************************************************************************/

/*
* 
* Practica 9
* Crear un script que imprima por pantalla el contenido de un array de 10 elementos.
* 1.- Crear un array de 10 elementos (cualquier tipo de valores).
* 2.- Agregar una explicación al código (Comentarios) que indique el proceso.
*/

// Escribir aqui la practica 9
Enter();
echo "((Practica 9))"; 
Enter();
$vector = array(1,2,3,4,5,6,7,8,9,10);// Generamos un vector
// recordemos que tambien se puede hacer asi: 
$vector2 = [1,2,3,4,5,6,7,8,9,10]; // Generamos un vector
// en el condicional podemos utilizar count($vector) para saber el numero de elementos del
// vector y recorrerlos todos dinamicamente (por si fuera necesario recorrer un array de mas 10 elementos)) 
// var_dump(count($vector)); // imprimimos el numero de elementos del vector, descomentar para ver el resultado
for($i=0;$i<10;$i++){ 
    print $vector[$i].' '; //Imprimimos el vector elemento a elemento
}
// Esta bien utilizar el for para recorrer el vector, pero lo mas seguro y sencillo es utilizar el foreach para recorrerlo
// ejemplo:
echo "<br>";
foreach($vector2 as $value){
    print $value.' ';
}

// En general bien hecho.


/*******************************************************************************************/

/*
* 
* Practica 10
* Crear un script que imprima por pantalla el contenido de un array asociativo de 10 elementos.
* 1.- Crear un array asociativo de 10 elementos (cualquier tipo de valores).
* 2.- Agregar una explicación al código (Comentarios) que indique el proceso.
*/

// Escribir aqui la practica 10
Enter();
echo "((Practica 10))";
Enter();
// observación: este array nunca se utiliza en tu codigo 
$vec2 = array(
    'Uno'=>1,
    'Dos'=>2,
    'Tres'=>3,
    'Cuatro'=>4,
    'Cinco'=>5,
    'Seis'=>6,
    'Siete'=>7,
    'Ocho'=>8,
    'Nueve'=>9,
    'Diez'=>10
    //Definimos el vector a imprimir 
);

// observacion: esto fue innecesario, ya que podemos utilizar el foreach para recorrer el array $vec2
$Aux = array('Uno','Dos','Tres','Cuatro','Cinco','Seis','Siete','Ocho','Nueve','Diez'); 
// Creamos una array con los nombres del array "principal"
foreach($Aux as $s => $vec2){
    echo $vec2. " ";   //Imprimimos cada elemento de vec2 usando el array Aux como indices
    // observacion: Aqui unicamente estamos recorriendo el array Aux
    // recordemos que en un foreach, el array que pasamos como parametro, el $s es el inidice y $vec2 el valor del elemento
}

// var_dump($vec2);
// die();
// en caso de querer recorrer el primer array con el segundo, podemos hacerlo asi:
echo "<hr>";
$vec2 = array( // vuelvo a crear el array $vec2 con los valores del array principal, por que en el foreach lo sobreescribes. descomenta el var_dump para comprobarlo.
    'Uno'=>1,
    'Dos'=>2,
    'Tres'=>3,
    'Cuatro'=>4,
    'Cinco'=>5,
    'Seis'=>6,
    'Siete'=>7,
    'Ocho'=>8,
    'Nueve'=>9,
    'Diez'=>10
);
for ($i=0; $i < count($vec2); $i++) { 
    if (array_key_exists($Aux[$i], $vec2)) {
        echo $vec2[$Aux[$i]]." ";
    }
}
echo "<hr>";


/*******************************************************************************************/

/*
* 
* Practica 11
* Crear un script que imprima por pantalla el contenido de un array multidimensional de 4x4 elementos.
* 1.- Crear un array multidimensional de 4x4 elementos (cualquier tipo de valores).
* 2.- Agregar una explicación al código (Comentarios) que indique el proceso.
*/

// Escribir aqui la practica 11
Enter();
echo "((Practica 11))";
Enter();
//Creamos el arreglo bidimensional de 4x4 elementos
$v1=array(1,2,3,4);
$v2=array(0,4,5,6);
$v3=array(0,0,1,0);
$v4=array(1,-1,0,10);
// Esto esta bien, pero buscaba que practicaran la estructura de un array multidimensional, esto para que en el futuro 
// puedan analizarlo y crear una estructura de datos mas compleja sin problemas.
$Matrix=array($v1,$v2,$v3,$v4); //Creamos el arreglo bidimensional de 4x4 elementos
$n=4;

for($i=0;$i<$n;$i++){
    for($j=0;$j<$n;$j++){
        echo $Matrix[$i][$j]." "; //Imprimimos la matriz mediante un recorrido dejado espacios y saltando lineas por estetica. 
    }
    Enter();
}



?>