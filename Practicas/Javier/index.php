<?php

/*
*  VALENZUELA MENDOZA FRANCISCO JAVIER
* Practica 1
* Crear una variable por cada tipo de dato permitido en PHP e imprimirla por pantalla.
* Agregar un comentario señalando el tipo de dato que se utiliza.
*
*/

$int = 5;
echo $int;
echo "<br>";
echo gettype($int);
echo "<br>";
// Integer (entero): números enteros.

$float = 7.3274;
echo $float;
echo "<br>";
echo gettype($float);
echo "<br>";
// Float: números decimales con maximo 7 digitos

$double = 9.32749724673;
echo $double;
echo "<br>";
echo gettype($double);
echo "<br>";
// Double: números decimales con maximo 15 digitos

$Phrase = 'First try';
echo $Phrase;
echo "<br>";
// String: cadenas de caracteres.

$T = true;
$F = false;
echo $T;
echo "<br>";
echo gettype($T);
echo "<br>";
// Boolean: valor lógico que solo admite verdadero falso.

$Colors = array("White","Blue","Green");
echo "<ul>";
for ($i = 0; $i < count($Colors); $i++) {
    echo "<li>$Colors[$i]</li>";
}
echo "</ul>";
echo "<br>";
// Array: conjunto de valores.

class Animal {
    public $Size;
    public $Type;
    public function __construct($Size, $Type) {
      $this->Size = $Size;
      $this->Type = $Type;
    }
    public function message() {
      return "My animal is a " . $this->Size . " " . $this->Type . "!";
    }
  }
  
  $myAnimal = new Animal("Small", "Cat");
  echo $myAnimal -> message();
  echo "<br>";
  $myAnimal = new Animal("Big", "Elephant");
  echo $myAnimal -> message();
  echo "<br>";
// Object: tipo especial de dato complejo.

$None = "Null Value";
$None = null;
var_dump($None);
echo "<br>";
// Null: valor que indica ausencia de valor.

/*******************************************************************************************/

/*
* 
* Practica 2
* Crear un script que imprima por pantalla todos los números pares del 1 al 100.
* Agregar una explicación al código (Comentarios) que indique el proceso.
* Utilizar bucle While o For.
*/

for($Num = 2; $Num <= 100; $Num = $Num + 2){ //Bucle inicia en 2, mientras el valor sea menos o igual a 100 continua, el valor se le suma dos
 
    echo $Num . "</br>"; // Imprime el valor
}

/*******************************************************************************************/

/*
* 
* Practica 3
* Crear un script que imprima los cuadrados (un número multiplicado por si mismo) de 
* los primeros 50 números.
* 1.- Utilizar bucle While o For.
* 2.- Agregar una explicación al código (Comentarios) que indique el proceso.
* 
*/

$Number = 0; //Contador inicial cero
while ($Number <= 50) { //Mientras que número del contador sea menor o igual a 50 continua el bucle
    echo $Number . " : " . $Number * $Number . "</br>"; // imprime el numero, y el numero al cuadrado
    
    $Number++; //Aumenta uno al contador
}

/*******************************************************************************************/

/*
* 
* Practica 4
* Crear un script que obtenga por la el metodo GET el valor de 2 variables y realizar
* todas las operaciones aritméticas basicas que se pueden realizar con ellas.
* 1.- Comprobar que se han recibido los valores por GET. En caso contrario, mostrar un 
* mensaje de error.
* 2.- Agregar una explicación al código (Comentarios) que indique el proceso.
*
*/
// ?num1=6&num2=2 --- Valores en url
$num1 = $_GET['num1']; //Obtiene primer valor
$num2 = $_GET['num2']; //Obtiene segundo valor

echo 'Suma: '.($num1 + $num2).'<br/>';

echo 'Resta: '.($num1 - $num2).'<br/>';

echo 'multiplicación: '.($num1 * $num2).'<br/>';

echo 'División: '.($num1 / $num2).'<br/>';

echo 'Resto: '.($num1 % $num2).'<br/>';

/*******************************************************************************************/

/*
* 
* Practica 5
* Crear un script que imprima por pantalla todos los números que hay entre dos números
* que se reciben por el metodo POST
* 1.- Comprobar que se han recibido los valores por POST. En caso contrario, mostrar un
* mensaje de error.
* 2.- Validar que los valores sean numéricos. En caso contrario, mostrar un mensaje de error.
* 3.- Validar que el segundo número sea mayor que el primero.
* 4.- Agregar una explicación al código (Comentarios) que indique el proceso.
*
*/

$num1 = $_GET['num1']; //Obtiene primer valor
$num2 = $_GET['num2']; //Obtiene segundo valor

for($I = $num1; $I <= $num2; $I++) { //contador igual a num1, contador es menos o igual a num2, suma valor de uno en uno
    echo $I . "</br>"; // Imprime el valor con salto
}

/*******************************************************************************************/

/*
* 
* Practica 6
* Imprimir todas las tablas de multiplicar del 1 al 10. en una tabla de HTML.
* 1.- Agregar una explicación al código (Comentarios) que indique el proceso.
*
*/

$multiplicando;
$multiplicador;

echo "<table text-align:center; border=5>";
echo "<tr>";
for ($tabla=1; $tabla<=10  ; $tabla++) { 
	echo "<td>Tabla del $tabla </td>";
}

echo "</tr>";
echo "<tr>";

for ($multiplicador=1; $multiplicador <=10 ; $multiplicador++) { 
	for ($multiplicando=01; $multiplicando <=10 ; $multiplicando++) { 
		echo "<td>$multiplicando X $multiplicador =";
		echo ($multiplicando *$multiplicador);
		echo "</td>";
	}
	echo "</tr>";
}
echo "</table>";

/*
* 
* Practica 7
* Crear un script que imprima por pantalla todos los números que hay entre dos números
* que se reciben por el metodo POST, pero estos números se deben mostrar separados por
* numeros pares y por numeros impares.
* 1.- Comprobar que se han recibido los valores por POST. En caso contrario, mostrar un
* mensaje de error.
* 2.- Validar que los valores sean numéricos. En caso contrario, mostrar un mensaje de error.
* 3.- Validar que el segundo número sea mayor que el primero.
* 4.- Agregar una explicación al código (Comentarios) que indique el proceso.
*
*/





/*
* 
* Practica 8
* Considera estás desarrollando una web donde trabajas con tipos de motor (suponemos que se 
* trata del tipo de motor de una bomba para mover fluidos). Define una variable $tipoMotor y 
* asígnale valor 3. Los valores posibles son 1, 2, 3, 4. A través de un condicional switch haz 
* lo siguiente:
* 
* a) Si el tipo de motor es 0, mostrar un mensaje indicando “No hay establecido un valor definido para el tipo de bomba”.
* b) Si el tipo de motor es 1, mostrar un mensaje indicando “La bomba es una bomba de agua”.
* c) Si el tipo de motor es 2, mostrar un mensaje indicando “La bomba es una bomba de gasolina”.
* d) Si el tipo de motor es 3, mostrar un mensaje indicando “La bomba es una bomba de hormigón”.
* e) Si el tipo de motor es 4,mostrar un mensaje indicando “La bomba es una bomba de pasta alimenticia”.
* f) Si no se cumple ninguno de los valores anteriores mostrar el mensaje “No existe un valor válido para tipo de bomba”.
*
*/

// Escribir aqui la practica 8

/*******************************************************************************************/

/*
* 
* Practica 9
* Crear un script que imprima por pantalla el contenido de un array de 10 elementos.
* 1.- Crear un array de 10 elementos (cualquier tipo de valores).
* 2.- Agregar una explicación al código (Comentarios) que indique el proceso.
*/

// Comenzamos el array con nombre Names
$Names = array(
    "Frank",
    "Louis",
    "Javier",
    "Tony",
    "Darren",
    "Blaine",
    "Sam",
    "Taylor",
    "James",
    "Roger");
echo "<ul>";
for ($i = 0; $i < count($Names); $i++) {  //contador empezar de cero, los recorre con el contador del array, y aumenta uno
    echo "<li>$Names[$i]</li>"; //Imprime el valor del array en forma de lista
}
echo "</ul>";
echo "<br>";

/*
* 
* Practica 10
* Crear un script que imprima por pantalla el contenido de un array asociativo de 10 elementos.
* 1.- Crear un array asociativo de 10 elementos (cualquier tipo de valores).
* 2.- Agregar una explicación al código (Comentarios) que indique el proceso.
*/
// Se crea un array de 10 elementos asociendandole un valor del primer tipo
$Estudiante = array(
    'Name' => 'Frank',
    'Last Name' => 'Valenzuela',
    'Age' => '23',
    'Phone' => '6629349830',
    'Career' => 'Systems',
    'ID' => '216201841',
    'Subjects' => '7',
    'Status' => 'Active',
    'Language' => 'English',
    'Year' => '2022',
);
echo $Estudiante['Name']; // Imprime el valor del array del tipo que se a seleccionado
echo "<br>";

/*******************************************************************************************/

/*
* 
* Practica 11
* Crear un script que imprima por pantalla el contenido de un array multidimensional de 4x4 elementos.
* 1.- Crear un array multidimensional de 4x4 elementos (cualquier tipo de valores).
* 2.- Agregar una explicación al código (Comentarios) que indique el proceso.
*/
// Array de 4x4 dandole valor numericos a cada uno de ellos
$valor [0] [0] = 1; $valor [0] [1] = 14; $valor [0] [2] = 8; $valor [0] [3] = 11;

$valor [1] [0] = 6; $valor [1] [1] = 19; $valor [1] [2] = 7; $valor [1] [3] = 16;

$valor [2] [0] = 3; $valor [2] [1] = 13; $valor [2] [2] = 4; $valor [2] [3] = 5;

$valor [3] [0] = 9; $valor [3] [1] = 15; $valor [3] [2] = 20; $valor [3] [3] = 30;

echo $valor [3] [3]; // Imprime el valor del array [3]x[3]


?>