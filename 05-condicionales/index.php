<?php

echo '<hr>';
echo 'CONDICIONALES';
echo '<br>';
echo '<h1> estructura IF</h1>';
echo '<hr>';
echo '<br>';

echo 'if(condicion){';
echo '<br>';
echo  '     instrucciones';
echo '<br>';
echo '}else{';
echo '<br>';
echo '    otras instrucciones';
echo '<br>';
echo '};';


echo '<br>';
echo '<hr>';
echo '<h1># Operadores de comparación</h1>';
echo '<hr>';
echo '<br>';
# Operadores de comparación
$num = '5';
$num2 = 5;
echo '== igual'; 
echo '<br>';
echo '=== identico';
echo '<br>';
echo '!= dieferente';
echo '<br>';
echo '<> diferente';
echo '<br>';
echo '!== no identico';
echo '<br>';
echo '< menor que (entre variables y valores)';
echo '<br>';
echo '> mayor que (entre variables y valores)';
echo '<br>';
echo '<= menor igual que';
echo '<br>';
echo '>= mayor igual que';

echo '<br>';
echo '<br>';
echo '<hr>';
echo '<h1>Ejemplo de uso de un if</h1>';
echo '<hr>';
echo '<br>';

// ejemplo de un if
$color = "5";

if($color == 5){
    echo "este color es rojo";
}else{
    echo "este color no es rojo";
};

echo '<br>';
echo '<hr>';
echo '<h1>ejemplos de comparación</h1>';
echo '<hr>';
echo '<br>';
$year = "2020";

// se puede probar este if con los operdores de condición
if($year == 2020){
    echo "estamos en 2020";
}else{
    echo "no estamos en 2020";
}

echo '<br>';
$nombre ="Luis Barraza";
$ciudad = "obregon";
$edad = "19";
$mayor_edad = "18";

echo "<br/>";

if($edad >= $mayor_edad){
    echo "$nombre es mayor de edad";
    echo "<br/>";
    if($ciudad == "hermosillo"){
        echo "y es de hermosillo";
    }else{
        echo "y es de $ciudad";
    }

}else{
    echo "$nombre no es mayor de edad";
    echo "<br/>";
    
    if($ciudad == "hermosillo"){
        echo "y es de hermosillo";
    }else{
        echo "y es de $ciudad";
    }
};

echo '<br>';
echo '<hr>';
echo '<h1>estructura de control ELSEIF</h1>';
echo '<hr>';
echo '<br>';
echo '<br>';
echo 'elseif(condicionales){';
echo '<br>';

// ejemplo elseif
echo '<br>';
echo '<hr>';
echo '<h1>ejemplo ELSEIF</h1>';
echo '<hr>';
echo '<br>';

$dia = "5";
if($dia == 1){
    echo "el dia es lunes";
}elseif($dia == 2){ // evaluacion 2 evaluo que sea igual a 2 y me muestre martes
    echo "el dia es martes";
}elseif($dia == 3){ // evaluacion 3
    echo "el dia es miercoles";
}elseif($dia == 4){ // evaluacion 4
    echo "el dia es jueves";
}elseif($dia == 5){
    echo "el dia es viernes";
}elseif($dia >= 6){
    echo "es fin de semana";
};


echo '<br>';
echo '<hr>';
echo '<h1># Operadores logicos</h1>';
echo '<hr>';
echo '<br>';

echo '&& AND (Y)';
echo '<br>';
echo '|| OR (O)';
echo '<br>';
echo '! - NOT (NO)';
echo '<br>';
echo 'and , or (Y. O)';
echo '<br>';

echo '<br>';
echo '<hr>';
echo '<h1># Ejemplos logicos</h1>';
echo '<hr>';
echo '<br>';

$edad_legal = 18;
$tercera_edad = 60;
$edad_real = 60;

if($edad_real >= $edad_legal && $edad_real <= $tercera_edad){
    echo "Tiene la edad para poder trabajar";
}else{
    echo "No tiene edad para trabajar";
};

echo "<br/>";
// ejemplo con OR
echo "<h4> ejemplo de un if con OR </h4>";
$pais = "EUA";

if($pais == "mexico" || $pais == "España" || $pais == "venezuela"){
    echo "En este pais se habla español";
}else{
    echo "En este pais no se habla español";
};

echo '<br>';
echo '<hr>';
echo '<h1># switch</h1>';
echo '<h4>estructura</h4>';
echo '<hr>';
echo '<br>';

echo 'switch ($variable a evaluar){';
echo '<br>';
echo '    case valor1:';
echo '<br>';
echo '        instrucciones';
echo '<br>';
echo '    break;';
echo '<br>';
echo '    case valor2:';
echo '<br>';
echo '        instrucciones';
echo '<br>';
echo '    break;';

echo '<br>';
echo '<hr>';
echo '<h1># Ejemplo switch</h1>';
echo '<hr>';
echo '<br>';

// esta estructura de control switch, es el mismo ejercicio que el elseif dias de la semana
echo "<h4> ejemplo de un switch </h4>";
$mes = '16';
switch ($mes){
    case 'enero':
        echo "Enero";
    break;
    case 2:
        echo "Febrero";
    break;
    case 3:
        echo "Marzo";
    break;
    case 4:
        echo "Abril";
    break;
    case 5:
        echo "Mayo";
    break;
    case 6:
        echo "Junio";
    break;
    case 7:
        echo "Julio";
    break;
    case 8:
        echo "Agosto";
    break;
    case 9:
        echo "Septiembre";
    break;
    case 10:
        echo "Octubre";
    break;
    case 11:
        echo "Noviembre";
    break;
    case 12:
        echo "Diciembre";
    break;
    default:
        echo "No es un mes valido su valor es $mes" ;
}

// if [ternaria]
echo '<br>';
$result = '';
($mes == 16 ? $result = 'Si' : $result = "no es 16");
echo $result;

echo '<br>';
echo '<hr>';
echo '<h1># GOTO</h1>';
echo '<hr>';
echo '<br>';
echo 'definición: salto de una instrucción a otra';
echo '<br>';

$user = 'fer';
if ($user == 'Admin'){
  GOTO marca;   
}
// Ejemplo de GOTO
// GOTO marca;
// al activar el GOTO se salta el codigo que esta entre el GOTO y la marca
echo "<h4> ejemplo de un GOTO </h4>";
echo "<h5> instruccion 1 </h5>";
echo "<h5> instruccion 2 </h5>";
echo "<h5> instruccion 3 </h5>";
echo "<h5> instruccion 4 </h5>";

// el nombre de la marca es variable
marca:
echo "<br/>";
// echo "me salte a 4 echos";
?>