<?php
/*
Arrays
un array es una colleccion o un conjunto de datos/valores, bajo un unico nombre.
Para acceder a esos valores podemos usar un indice numerico o alfanumerico.
*/
// esto es una variable
$pelicula = "Batman";

$peliculas = array(
    'Batman', 
    'Spiderman',
    'El señor de los anillos'
);

$cantantes = [
    '2pac', 
    'Dreake', 
    'Jennifer Lopez'
];

var_dump($peliculas);
echo "<hr/>";
echo count($peliculas);
echo "<br/>";
//Se escribe el indice del valor que se desea imprimir entre corchetes
echo $peliculas[0];

// recorrer array con bucle for
echo "<h3>Listado de peliculas</h3>";
echo "<ul>";
for ($i = 0; $i < count($peliculas); $i++) {
    echo "<li>$peliculas[$i]</li>";
}
echo "</ul>";

// recorrer array con bucle foreach
echo "<h3>Listado de cantantes</h3>";
echo "<ul>";
foreach ($cantantes as $cantante) {
    echo "<li>$cantante</li>";
}
echo "</ul>";
$peliculas = array(
    'Batman', 
    'Spiderman',
    'El señor de los anillos'
);

// Array Asociativo (alfanumerico)
$personas = array(
    'Nombre' => 'luis',
    'apellido' => 'Barraza',
    'edad' => '29',
    'telefono' => '6624124560',
);
echo $personas['Nombre'];

//Array multidimensionales
$contactos = array(
    array(
        'nombre' => 'Luis',
        'email' => 'luis@gmail.com'
    ),
    array(
        'nombre' => 'Jose',
        'email' => 'jose@gmail.com'
    ),   
    array(
        'nombre' => 'Pedro',
        'email' => 'pedro@gmail.com'
        )
    );
    echo "<br>";
    
    $arrayMultiple = [
        'array1' => [
            'nombre' => 'Luis',
            'email' => 'luis@',
        ],
        'array2' => [
            'nombre' => 'Jose',
            'email' => 'luis',
        ],
    ];
    var_dump($contactos);
    var_dump($arrayMultiple);
    
    //acceder a los datos dentro de un array multidimensional
    echo $contactos[0]['email'];
    echo "<br>";
    
echo "<hr/>";
foreach ($contactos as $key => $value) {
    // echo 'esto es una indice: ' . $key . '<br>';
    // echo 'esto es un valor: ' . $value . '<br>';
    var_dump($value);
    foreach ($value as $key => $valor) {
        echo 'esto es una indice: ' . $key . ' Y esto su valor: ' . $valor . '<br>';
    }
}


