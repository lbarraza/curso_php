<?php
/*
VARIABLES LOCALES:
Son las que se definen dentro de una funcion y no pueden ser usadas fuera de la funcion,
solo estan disponibles dentro de la misma funcion. A no ser que hagamos un return.

VARIALES GLOBALES:
Son las que se declaran fuera de una funcion y estan disponibles dentro y fuera de las funciones.
*/

// Variable global
$texto = "esta es una variable global";

echo $texto;

function variableGlobal(){
    // Declaramos que la variable $texto es una variable global
    // para poder utilizarla o acceder a su contenido
    global $texto;
    echo "<h3>$texto</h3>";

    // Variable local
    $year = 2020;
    // echo $year;

    // Se hace un return para poder mostrar el contenido desde fuera de la funcion
    return $year;
}

 echo variableGlobal();

// Esta es una variable local por que se declaro dentro de una funcion
echo $year; //envia error al querer mostrarla en el echo

?>