<?php
/*
FUNCIONES:
Una función es un conjunto de instrucciones agrupadas bajo un nombre concreto y que pueden 
reutilizarse unicamente llamando al nombre de la función tantas veces como sea necesario.

ESTRUCTURA:
function miFuncion($mi_parametro){
    bloque de instrucciones
}

INVOCARLA
miFuncion('parametro');
*/
// $validar_formulario_html = true;
// $validateFormsHtml = true;
// function miFuncion($numero){
//     $numero1 = 10;
//     echo $numero1 + $numero;
// }

// miFuncion(10);

// echo "<br/>";

// function tabla($num){
//     for ($y=1; $y <= 10 ; $y++) { 
//         echo "$num x $y =" .($num*$y). "<br/>";
//     }
// }

// tabla(2);

// echo "<br/>";

function tablaFor($num){
    for ($i=1; $i <=10 ; $i++) { 
        echo "$num x $i =" .($num*$i). "<br/>";
    }
}

// for ($x=1; $x <=20; $x++) { 
//     tablaFor($x);
//     echo "<br/>";
// }
//  echo "<hr/>";


 function calculadora($numero1, $numero2, $opcional = false){
    //  instrucciones
    $suma = $numero1+$numero2;
    $resta = $numero1-$numero2;
    $multiplicacion = $numero1*$numero2;
    $division = $numero1/$numero2;
    $resto = $numero1%$numero2;

    if ($opcional) {
        echo "<h3>";
    }
    echo "Suma = $suma <br/>";
    echo "Resta = $resta <br/>";
    echo "Multiplicación = $multiplicacion <br/>";
    echo "División = $division <br/>";
    echo "Resto = $resto <br/>";
    echo "<hr/>";

    if ($opcional) {
        echo "</h3>";
    }
 }

// calculadora(1,5);
// calculadora(2,6,true);
// calculadora(3,7, 5);
// calculadora(3,7);
// calculadora(4,8);

// PARAMETROS OPCIONALES

function parametrosOpcionales($parametro_opcional = 'Luis'){
    echo $parametro_opcional;
    echo "<hr/>";
}
// Si el parametro no se envia imprimira Luis
parametrosOpcionales('david');


// RETURN
function calculadora2($numero1, $numero2, $opcional = false){
    //  instrucciones
    $suma = $numero1+$numero2;
    $resta = $numero1-$numero2;
    $multiplicacion = $numero1*$numero2;
    $division = $numero1/$numero2;
    $resto = $numero1%$numero2;

    $cadena = '';

    if ($opcional) {
        $cadena .= "<h3>";
    }
    $cadena .= "Suma = $suma <br/>";
    $cadena .= "Resta = $resta <br/>";
    $cadena .= "Multiplicación = $multiplicacion <br/>";
    $cadena .= "División = $division <br/>";
    $cadena .= "Resto = $resto <br/>";
    $cadena .= "<hr/>";

    if ($opcional) {
        $cadena .= "</h3>";
    }

    return $cadena;
 }


function evaluar($x = 1){
    if($x != 1 ){
        $y = 'Si se envio';
    }else {
        $z = 'No se envio';
    }

    if (isset($y)) {
        return 2;
    }elseif(isset($z)){
        return 1;
    }
}

// print_r(evaluar());
// print_r(calculadora2(1,5));
// calculadora2(1,5);
// print calculadora2(4,2);

function getName($name){
    $texto = "Mi nombre es $name";
    return $texto;
}

// echo getName('Luis');
// Utilizar una función dentro de otra función
function unaDentroDeOtra($name,$lastname){
    // aqui incluimos la funcion anterior y le pasamos la variable $name
    $texto = getName($name)
            ."<br/>".
            // aqui concatenamos la variable $lastname
            "Mi apellido es $lastname"
            ."<hr/>";
    return $texto;
}

echo unaDentroDeOtra('Fernando', 'Barraza');
// echo "<hr/>";

?>