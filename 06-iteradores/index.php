<?php
/*
BUCLES
El Bucle es una estructura de control que itera o 
repite la ejecución de una serie de instrucciones
muchas veces o tantas veces sea necesario.
Esto se ejecuta en base a una condición

Bucle WHILE
while ($a <= 10 / condición) {
    # codigo/instrucciones...
}
*/

// Ejemplo de bucle WHILE
echo "<h3> Ejemplo de un WHILE <h3/>";
$num = 0;
while ($num <= 30) {
    echo $num;
    if ($num < 30) {
        echo ", ";
    }
    $num++;
}
echo "<hr/>";
// ejemplo
/*Aqui consultamos si nos llega el valor de la variable num por $_GET por la url si da true
ejecutamos la instruccion, donde creamos una variable $num2 y le asignamos el valor que recibimos por $_GET.
Si no existe ejecutamos el else, donde asignamos un valor estatico a la variable.
#isset comprueba si existe o no una variable en el codigo o un indice en un array.
#(tipo de datos) antes del valor recibido (castin / castear una variable) que es cambiar el tipo de dato
*/
if(isset($_GET['num'])){
    $num2 = (int) $_GET['num'];
}else{
    $num2 = 10;
}

// var_dump($num2);
echo "<h3>Tabla de multiplicar del número $num2 por medio de un WHILE<h3/>";
$count = 0;
while ($count <= 10) {
    if ($num2 != 1 && $num2 != 10) {
        echo  "$num2 x $count = " . ($num2 * $count) . "<br/>";
    }
    // aqui aumentamos $count en 1 en cada iteración
    $count++;
}

echo "<hr/>";

// $num3 = 6;
// $num3+$num3++; 
// echo $num3;

// $num4 = 6;
// $num4++;
// echo $num4;

// /*
// Bucle DO WHILE
// do {
//     # codigo / instrucciones...
// } while ($a <= 10 / condición);
// */
echo "<h3> Ejemplo de un DO WHILE <h3/>";
$edad = 18 ;
$local = 1 ;
do {
    // estas instrucciones se ejecutaran una vez si o si
    echo "tienes acceso al local $local <br/>";
    $local++;
    // La condición while se ejecuta despues de las instrucciones
    // Si la condicion se cumple lo ejecuta de nuevo, si no se detiene y ejecuta una sola vez.
} while ($edad >= 18 && $local <= 5);

echo "<hr/>";


// /* 3 parametros - variable contador, condicion, incremento */
// Bucle FOR
// for ($i=0 /variable contador; $i < / condición; $i++ /actualización del contador) { 
//     # codido / instrucciones...
// }
// */

// Ejemplo de un FOR
echo "<h3> Ejemplo de un FOR <h3/>";
$resultado = 0 ;
/* iniciamos el contador en 0, mientras que el contador sea menor igual a la condición se ejecutara nuevamente
y el contador incrementara en 1 en cada iteración
*/
for ($i=0; $i <= 5 ; $i++) { 
    // Aqui la variable sera igual al valor de la variable mas el valor del contador (el valor de la condición).
    echo $resultado;
    echo '<br/>';
    echo $i;
    echo '<br/>';
    $resultado += $i;
    // Aqui si queremos podemos imprimir cada resultado que se va iterando (descomentar el codigo para ver el resultado)
    // echo "$resultado";
}
echo "El resultado de la suma del FOR es: $resultado";

echo "<hr/>";
$num5 = $_GET['num5'];
echo "<h3>Tabla de multiplicar del número $num2 por medio de un FOR, 
añadimos un IF con un BREACK si es la tabla del 5<h3/>";
for($count = 0 ; $count <= 10; $count++) {
    if ($num5 == 5){
        echo "la table del 5 no se puede mostrar";
        break;
    }
    echo "$num5 x $count =" . ($num5 * $count) . "<br/>";
}

echo "<hr/>";

?>