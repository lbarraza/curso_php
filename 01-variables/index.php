<?php
echo "<h3> Variables en PHP <h3>";

$var = "esto es una variable";
echo $var;

echo "<br>";
echo "NOTA: si existen 2 o más variables iguales php imprimira solo el valor de la ultima variable declarada";

echo "<h3> Tipos de Datos (descritos en el codigo interno) <h3>";
/*
Tipos de datos
Entero (int / integer) = 99
coma flotante /decimales (float /double) = 3.45
cadena (string) = hola soy una cadena de datos
Boleano (bool) = true o false [1 o 0]
null = sin datos inexistente
array (coleccion de datos)
objetos
*/

//gettype es una funcion para obtener el tipo de dato

// entero
$int = 100;
echo $int;
echo "<br>";
echo gettype($int);
echo "<br>";

// decimales
$float = 3.49;
echo $float;
echo "<br>";
echo gettype($float);
echo "<br>";

// cadena de texto
$string = "esto es una cadena de texto";
echo $string;
echo "<br>";
echo gettype($string);
echo "<br>";

// Boleano
$bool = true;
echo $bool;
echo "<br>";
echo gettype($bool);
echo "<br> ALT + 92";
echo "<br> este es un sting \"comillas\" dobles";
echo '<br> este es un sting \'comillas\' simples';
echo '<br> este es un sting "comillas" simples';
// concatenar
echo '<br>'. $float . $bool;
/*
CURIOSIDADES DE LAS VARIABLES
1.- Una variable no puede iniciar por un numero, pero puede terminar en uno o contenerlo.
2.- Podemos separar el nombre con guion bajo (_)
3.- NO puede contener signos de operadores (+, -, *, / etc..)
4.- NO debe contener caracteres especiales (comillas, tildes etc)
5.- Para concatenar variables en textos o en variables se utiliza .$variable dentro de comillas simples.
    en comillas dobles no es necesario concatenar.
6.- Las comillas simples son mas rapidas a nivel de procesamiento que las comillas dobles.
7.- Para escapar comillas dobles en un string se utiliza \ antes de las comillas (\").
*/

// para debugear una variable se utiliza var_dump();
// se obtiene el tipo de datos, el contenido y la longitud.
var_dump($float);
echo "<br>";

// variables superglobales
// servidor
echo $_SERVER['SERVER_ADDR'];
echo "<br>";
echo $_SERVER['SERVER_NAME'];
echo "<br>";
echo $_SERVER['SERVER_SOFTWARE'];
echo "<br>";
echo $_SERVER['REMOTE_ADDR'];
echo "<br>";
// Se puede investigar mas sobre las variables superglobales y sus usos




?>