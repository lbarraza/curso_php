<?php

// CONSTANTES
// Son parecidas a las variables, pero; el valor de la constante NUNCA puede variar.


// Se define de la siguiente forma "define('nombre de la constante', 'contenido');"
define('nombre', 'Luis Barraza');
// Se imprime de esta forma
echo nombre;
// o concatenadas asi
echo "<h3>".nombre."<h3>";

// variable predifinidas ejemplos
echo PHP_VERSION; // version de php
echo "<br>";
echo PHP_OS; // sistema operativo
echo "<br>";
// esta indica el numero de linea del codigo
echo __LINE__;
echo "<br>";
// esta indica el directorio completo del documento
echo __FILE__;
echo "<br>";
// esta indica el nombre de la funcion en la que se encuentre
echo __FUNCTION__;
echo "<br>";

// Se puede investigar sobre mas constantes utiles para utilizar al momento de programar


?>