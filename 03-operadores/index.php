<?php
//  operadores aritmeticos (+, -, *, /)
$num1 = 77;
$num2 = 23;

echo 'Suma: '.($num1 + $num2).'<br/>';

echo 'Resta: '.($num2 - $num1).'<br/>';

echo 'multiplicación: '.($num1 * $num2).'<br/>';

echo 'División: '.($num1 / $num2).'<br/>';

echo 'Resto: '.($num1 % $num2).'<br/>';

// operadores de incremento y decremento

$year = 2019;

// incremento aumenta 1
$year++;
echo $year;
echo "<br/>";

// decremento disminuye 1
$year--;
echo $year;
echo "<br/>";

// pre-incremento = ( 1 + $year) 
++$year;
echo $year;
echo "<br/>";

// pre-decremento = ( 1 - $year) 
--$year;
echo $year;
echo "<br/>";

// operadores de asignación. por ejemplo el "=".
$edad = 29;
echo $edad;
echo "<br/>";

// con este operador le añadimos/actualizamos la variable 5 mas ($edad = $edad + 5)
echo ($edad+=5);
echo "<br/>";
// debugeamos la variable para confirmar la operación
var_dump($edad);
echo "<br/>";
// esto se puede realizar con cualquier otro operador aritmetico



?>